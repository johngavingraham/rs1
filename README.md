# RS1

A fancy prompt.


## TODO

Virtual environment!


## Building

Install [rust](https://www.rust-lang.org/tools/install), then build
and install:

```bash
cargo install --path=.
```


## Usage

Source the `rs1.sh` file from startup code - e.g. from your `~/.bashrc`:

```bash
source /path/to/rs1.sh
```
