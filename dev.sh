# Source in order to use the debug version of the binary as your
# prompt.

RS1_DEV_PATH=$(realpath $(dirname ${BASH_SOURCE[0]}))

export PATH=${RS1_DEV_PATH}/target/debug:${PATH}
source ${RS1_DEV_PATH}/rs1.sh
