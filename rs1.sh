unset PROMPT_COMMAND
export PS0="\$(rs1 ps0 $$)"
export PS1="\$(rs1 ps1 $$ \$? '\u' '\h' '\w')"

# Keys to ignore in the environment - e.g. if we don't want to get
# notified every time we change directory:
export RS1_IGNORE_KEYS='OLDPWD:PWD:COLUMNS'
