#![allow(unused)]

use std::{
    collections::{HashMap, HashSet},
    env,
    error::Error,
    ffi::CStr,
    fs,
    ops::Add,
    os::unix::process::ExitStatusExt,
    path::PathBuf,
    process::ExitStatus,
    str::FromStr,
};

use chrono::{DateTime, Duration, Local, Utc};
use clap::{Parser, Subcommand};
use colored::{control::SHOULD_COLORIZE, Color, ColoredString, Colorize, CustomColor};
use directories::{ProjectDirs, UserDirs};
use git2::{BranchType, Repository, Status};
use libc::{strsignal, SIGINT, SIGUSR1};
use log::{debug, error};
use pathdiff::diff_paths;
use serde::{Deserialize, Serialize};

#[derive(Debug, Parser)]
pub struct Cli {
    #[command(subcommand)]
    stage: Stage,
}

#[derive(Debug, Subcommand)]
pub enum Stage {
    Ps0 {
        ppid: i32,
    },
    Ps1 {
        ppid: i32,
        exit_code: i32,
        user: String,
        host: String,
        work_dir: String,
    },
}

#[derive(Deserialize, Serialize)]
struct PsInfo {
    start_time: DateTime<Utc>,
    env: HashMap<String, String>,
}

impl PsInfo {
    fn new() -> Self {
        Self {
            start_time: Utc::now(),
            env: env::vars().collect(),
        }
    }
}

fn main() {
    SHOULD_COLORIZE.set_override(true);
    env_logger::init();

    //for i in 0..255 {
    //    println!("{}", "true color".truecolor(0, i, 0));
    //}

    let cli = Cli::parse();

    debug!("cli: {:?}", cli);

    let result = match cli.stage {
        Stage::Ps0 { ppid } => ps0(ppid),
        Stage::Ps1 {
            ppid,
            exit_code,
            user,
            host,
            work_dir,
        } => ps1(ppid, exit_code, user, host, work_dir),
    };

    match result {
        Ok(_) => {}
        Err(e) => error!("{}", e),
    }
}

fn ps0(ppid: i32) -> Result<(), Box<dyn Error>> {
    debug!("ps0 (ppid={ppid})");

    fs::write(
        get_runtime_path(ppid)?,
        serde_json::to_string_pretty(&PsInfo::new())?,
    )?;

    Ok(())
}

fn get_runtime_path(ppid: i32) -> Result<PathBuf, Box<dyn Error>> {
    let project_dirs =
        ProjectDirs::from("com", "johngavingraham", "rs1").ok_or("failed to get project dirs")?;
    let runtime_dir = project_dirs.data_local_dir();

    fs::create_dir_all(runtime_dir)?;

    let runtime_path = runtime_dir.join(format!("{ppid}"));

    debug!("runtime path: {runtime_path:?}");

    Ok(runtime_path.to_path_buf())
}

fn git_status() -> Option<ColoredString> {
    if let Ok(mut repo) = Repository::discover(".") {
        let mut nbranches_1 = 0;
        let mut nbranches_2 = 0;
        for (nbranches_1, branch) in repo.branches(Some(BranchType::Local)).ok()?.enumerate() {
            //nbranches_1 += 1;
            if let Ok(b2) = &branch {
                nbranches_2 += 1;

                match b2.0.upstream() {
                    Err(e) => debug!("branch {:?}: no upstream ({:?})", b2.0.name().ok()?, e),
                    Ok(remote) => {
                        debug!("branch {:?}: upstream is {:?}", b2.0.name(), remote.name())
                    }
                }
            }
        }

        let mut stashes = 0;
        repo.stash_foreach(|_, _, _| {
            stashes += 1;
            true
        });

        let branch = repo.head().ok()?;

        let name = branch.shorthand()?;

        let statuses = repo.statuses(None).ok()?;

        let mut index_set: HashSet<char> = HashSet::new();
        let mut wt_set: HashSet<char> = HashSet::new();
        let mut conflicted_set: HashSet<char> = HashSet::new();

        for status in &statuses {
            let status = status.status();

            if status.contains(Status::INDEX_NEW) {
                index_set.insert('N');
            }
            if status.contains(Status::INDEX_MODIFIED) {
                index_set.insert('M');
            }
            if status.contains(Status::INDEX_DELETED) {
                index_set.insert('D');
            }
            if status.contains(Status::INDEX_RENAMED) {
                index_set.insert('R');
            }
            if status.contains(Status::INDEX_TYPECHANGE) {
                index_set.insert('T');
            }

            if status.contains(Status::WT_NEW) {
                wt_set.insert('N');
            }
            if status.contains(Status::WT_MODIFIED) {
                wt_set.insert('M');
            }
            if status.contains(Status::WT_DELETED) {
                wt_set.insert('D');
            }
            if status.contains(Status::WT_RENAMED) {
                wt_set.insert('R');
            }
            if status.contains(Status::WT_TYPECHANGE) {
                wt_set.insert('R');
            }

            if status.contains(Status::CONFLICTED) {
                conflicted_set.insert('C');
            }
        }

        let mod_strings = [index_set, wt_set, conflicted_set].map(|s| -> String {
            let chs: Vec<char> = s.into_iter().collect();

            let s: String = chs.into_iter().collect();

            s.yellow().to_string()
        });

        let stashes = if stashes == 0 {
            ";".bright_white()
        } else {
            format!(";{}", format!("S={}", stashes).yellow()).bright_white()
        };

        let mod_string =
            format!("git:{}({};{})", name.red(), mod_strings.join(","), stashes).bright_white();

        Some(mod_string)
    } else {
        None
    }
}

fn get_ignore_keys() -> Vec<String> {
    match env::var("RS1_IGNORE_KEYS") {
        Ok(ignore_keys) => ignore_keys.split(':').map(|s| s.to_owned()).collect(),
        Err(_) => vec![],
    }
}

fn remove_keys(map: &mut HashMap<String, String>, keys: &Vec<String>) {
    for key in keys {
        map.remove(key);
    }
}

fn print_env_diff(prev: &mut HashMap<String, String>) {
    let ignore_keys = get_ignore_keys();

    let mut curr: HashMap<String, String> = env::vars().collect();

    remove_keys(prev, &ignore_keys);
    remove_keys(&mut curr, &ignore_keys);

    let prev_keys: HashSet<_> = prev.keys().collect();
    let curr_keys: HashSet<_> = curr.keys().collect();

    let new_keys = curr_keys.difference(&prev_keys);
    let deleted_keys = prev_keys.difference(&curr_keys);

    let same_keys = curr_keys.intersection(&prev_keys);

    let grey = 100;
    let unset = "(unset)".custom_color(CustomColor::new(grey, grey, grey));

    for new_key in new_keys {
        println!(
            "{}: {} -> {}",
            new_key.blue(),
            unset,
            // Implicit unwrap() is fine because we've seen this in
            // this hashset.
            curr[new_key.as_str()].green(),
        );
    }

    for key in same_keys {
        // Implicit unwrap()s are fine because we've seen this in
        // these hashsets.
        let prev_value = &prev[key.as_str()];
        let curr_value = &curr[key.as_str()];

        if prev_value != curr_value {
            let curr_value_abridged = if curr_value.contains(prev_value) {
                curr_value.replace(prev_value, &format!("${{{}}}", key))
            } else {
                curr_value.to_string()
            };

            let prev_value_abridged = if prev_value.contains(curr_value) {
                prev_value.replace(curr_value, &format!("${{{}}}", key))
            } else {
                prev_value.to_string()
            };

            println!(
                "{}: {} -> {}",
                key.blue(),
                prev_value_abridged.red(),
                curr_value_abridged.green(),
            );
        }
    }

    for deleted_key in deleted_keys {
        println!(
            "{}: {} -> {}",
            deleted_key.blue(),
            // Implicit unwrap() is fine because we've seen this in
            // this hashset.
            prev[deleted_key.as_str()].red(),
            unset,
        );
    }
}

fn print_ps_info(ppid: i32, exit_code: i32) -> Result<(), Box<dyn Error>> {
    let exit_code = exit_status_string(exit_code);

    let runtime_path = get_runtime_path(ppid)?;

    if runtime_path.exists() {
        let ps_info = fs::read_to_string(runtime_path.clone())?;

        match fs::remove_file(runtime_path) {
            Ok(_) => (),
            Err(e) => error!("{}", e),
        }

        let mut ps_info: PsInfo = serde_json::from_str(&ps_info)?;

        print_env_diff(&mut ps_info.env);

        let duration = Utc::now() - ps_info.start_time;

        let hours = duration.num_hours();
        let minutes = duration.num_minutes() % 60;
        let seconds = duration.num_seconds() % 60;
        let milliseconds = duration.num_milliseconds() % 1000;
        let deciseconds = milliseconds / 100;

        let duration = if hours == 0 && minutes == 0 && seconds == 0 {
            format!("0.{:003}s", milliseconds)
        } else if hours == 0 && minutes == 0 {
            format!("{}.{}s", seconds, deciseconds)
        } else if hours == 0 {
            format!("{:02}m{:02}s", minutes, seconds)
        } else {
            format!("{:02}h{:02}m{:02}s", hours, minutes, seconds)
        };

        let output = format!("[{exit_code} ({duration})]");

        let grey = 100;
        println!(
            "{}",
            output.custom_color(CustomColor::new(grey, grey, grey))
        );
    }

    Ok(())
}

/// This is an exit code that's used by e.g. `git` when you run `git
/// branch <branch-name-that-exists>`.
static MAX_EXIT_CODE: i32 = 128;

fn exit_status_string(exit_code: i32) -> ColoredString {
    let exit_status = ExitStatus::from_raw(exit_code);
    debug!("exit status: {exit_status}");
    if let Some(signal) = exit_status.signal() {
        let name = unsafe { strsignal(signal) };

        let c_str = unsafe { CStr::from_ptr(name) };

        debug!("{:?}", &c_str);
    }

    if exit_code > MAX_EXIT_CODE {
        if let Some(signal) = exit_status.signal() {
            let name = unsafe { strsignal(signal) };

            let c_str = unsafe { CStr::from_ptr(name) };

            format!(
                "{} (signal {})",
                c_str.to_str().unwrap_or("[INVALID STRING]"),
                signal
            )
            .black()
            .on_red()
        } else {
            format!("Unknown exit code: {}", exit_code).black().on_red()
        }
    } else if exit_code == 0 {
        "OK: 0".green()
    } else {
        format!("Error: {}", exit_code).red()
    }
}

fn ps1(
    ppid: i32,
    exit_code: i32,
    user: String,
    host: String,
    work_dir: String,
) -> Result<(), Box<dyn Error>> {
    #[cfg(debug_assertions)]
    println!("(debug)");

    match print_ps_info(ppid, exit_code) {
        Ok(_) => {}
        Err(e) => error!("error printing PS info: {e}"),
    }

    let git_string = git_status();

    let datetime = Local::now();
    let date_string = datetime.format("%Y:%m:%d").to_string();
    let time_string = datetime.format("%H:%M:%S").to_string();
    let datetime_string = format!(
        "{}T{}",
        date_string.bright_white(),
        time_string.bright_white()
    )
    .bright_black();

    let mut status = format!("{}", datetime_string).purple();

    if let Some(git_string) = git_string {
        status = format!("{} | {}", status, git_string).purple()
    };

    let work_dir_expanded = shellexpand::tilde(&work_dir).into_owned();
    if let Ok(virtual_env) = env::var("VIRTUAL_ENV") {
        let path =
            if let Some(path_diff) = diff_paths(virtual_env.clone(), work_dir_expanded.clone()) {
                path_diff.to_owned().to_string_lossy().to_string()
            } else {
                virtual_env
            };
        status = format!("{} | {}({})", status, "venv:".bright_white(), path.blue()).purple();
    } else {
        if let Ok(mut dir) = env::current_dir() {
            // So the while-loop works correctly.
            dir.push("token");

            while (dir.pop()) {
                let to_glob = format!("{}/*/bin/activate", dir.to_string_lossy());
                if let Ok(activate) = glob::glob(to_glob.as_str()) {
                    let activate: Vec<String> = activate
                        .into_iter()
                        .filter_map(|path_result: Result<PathBuf, _>| {
                            if let Ok(path) = path_result {
                                if let Some(s) = diff_paths(path, work_dir_expanded.clone()) {
                                    String::from_str(&s.to_string_lossy()).ok()
                                } else {
                                    None
                                }
                            } else {
                                None
                            }
                        })
                        .collect();

                    if !activate.is_empty() {
                        status = format!(
                            "{} | {}({})",
                            status,
                            "potential venv".bright_blue(),
                            activate.join(",").yellow()
                        )
                        .purple();

                        break;
                    }
                }
            }
        }
    }

    let status = format!("[{}]", status).purple();

    println!("{}", status);

    let work_dir = {
        let user_dirs = UserDirs::new();

        if let Some(user_dirs) = user_dirs {
            let home_dir = user_dirs.home_dir().to_string_lossy().to_string();

            if work_dir.starts_with(&home_dir) {
                work_dir.replacen(&home_dir, "~/", 1).yellow()
            } else {
                work_dir.yellow()
            }
        } else {
            work_dir.yellow()
        }
    };

    let color = if user == "root" {
        Color::Red
    } else {
        Color::Yellow
    };

    let prompt_char = if user == "root" { "#" } else { "$" };

    let ps1 = format!(
        "[{}@{}:{}]",
        user.color(color),
        host.color(color),
        work_dir.color(color),
    )
    .purple();

    // Don't colour prompt_char, some terminals don't like it.
    print!("{}\n{} ", ps1, prompt_char);

    Ok(())
}
